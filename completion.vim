Plug 'neoclide/coc.nvim', { 'branch': 'release' }

inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

inoremap <silent><expr> <C-space> coc#refresh()

"GoTo code navigation
"nmap <leader>g <C-o>
"nmap <silent> gd <Plug>(coc-definition)
"nmap <silent> gt <Plug>(coc-type-definition)
"nmap <silent> gi <Plug>(coc-implementation)
"nmap <silent> gr <Plug>(coc-references)
"
"nmap <leader>rn <Plug>(coc-rename)
"
""show all diagnostics.
"nnoremap <silent> <space>d :<C-u>CocList diagnostics<cr>
"manage extensions.
"nnoremap <silent> <space>e :<C-u>CocList extensions<cr>
