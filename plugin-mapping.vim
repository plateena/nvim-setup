nmap <C-p> :Files<Cr>
nmap <leader>b :Buffers<Cr>

nmap <C-e> :NERDTreeToggle<Cr>

map <Leader>s <Plug>(easymotion-bd-f)
map <Leader>ww <Plug>(easymotion-bd-w)
map <leader>ss <Plug>(easymotion-s2)
map <leader>st <Plug>(easymotion-t2)
map <Leader>l <Plug>(easymotion-lineforward)
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)
map <Leader>h <Plug>(easymotion-linebackward)
