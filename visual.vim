Plug 'ryanoasis/vim-devicons'                   " Show filetype icon on nerdtree
Plug 'flazz/vim-colorschemes'                   " Color theme for the vim
Plug 'vim-airline/vim-airline'                  " For the status line visual
Plug 'vim-airline/vim-airline-themes'
Plug 'ericbn/vim-relativize'                    " Display number to jump or relative number from current line
set relativenumber

Plug 'Xuyuanp/nerdtree-git-plugin'              " Git plugin Tree navigation on the side
Plug 'vwxyutarooo/nerdtree-devicons-syntax'     " Color for the nerdtree
Plug 'markonm/traces.vim'                       " show preview of subtitue command
Plug 'sheerun/vim-polyglot'                     " Syntax highlight
Plug 'chrisbra/Colorizer'                       " Display color for rgb and hex
Plug 'qpkorr/vim-bufkill'                       " Remove buffer without closing vim window

Plug 'junegunn/limelight.vim'                   " Highlight current paragraphs
" Color name (:help cterm-colors) or ANSI code
let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240

" Math html tag
Plug 'Valloric/MatchTagAlways'                  " Match HTML always
let g:mta_use_matchparen_group = 1
let g:mta_set_default_matchtag_color = 1
let g:mta_filetypes = {
    \ 'vue' : 1,
    \ 'html' : 1,
    \ 'xhtml' : 1,
    \ 'xml' : 1,
    \ 'jinja' : 1,
    \}

hi MatchParen cterm=bold ctermfg=8 ctermbg=8 guibg=None

" Colorscheme
Plug 'mhartington/oceanic-next'
Plug 'rakr/vim-one'
Plug 'drewtempelmeyer/palenight.vim'
Plug 'ayu-theme/ayu-vim'
Plug 'morhetz/gruvbox'
Plug 'sonph/onehalf', {'rtp': 'vim/'}
"good colorschemes: murphy, slate, molokai, badwolf, solarized, nord, one,
"oceanic, gruvbox, ayu, one, palenight, tender
