let g:airline_theme='ayu_mirage'
" let g:airline_theme='dracula'

" let g:airline_symbols = {}

" unicode symbols
"let g:airline_left_sep = '»'
"let g:airline_left_sep = '▶'
"let g:airline_right_sep = '«'
"let g:airline_right_sep = '◀'
let g:airline_symbols_crypt = '🔒'
let g:airline_symbols_linenr = '☰'
let g:airline_symbols_linenr = '␊'
let g:airline_symbols_linenr = '␤'
let g:airline_symbols_linenr = '¶'
let g:airline_symbols_maxlinenr = ''
let g:airline_symbols_maxlinenr = '㏑'
let g:airline_symbols_branch = '⎇'
let g:airline_symbols_paste = 'ρ'
let g:airline_symbols_paste = 'Þ'
let g:airline_symbols_paste = '∥'
let g:airline_symbols_spell = 'Ꞩ'
let g:airline_symbols_notexists = 'Ɇ'
let g:airline_symbols_whitespace = 'Ξ'

" powerline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols_branch = ''
let g:airline_symbols_readonly = ''
let g:airline_symbols_linenr = '☰'
let g:airline_symbols_maxlinenr = ''
let g:airline_symbols_dirty='⚡'

" old vim-powerline symbols
"let g:airline_left_sep = '⮀'
"let g:airline_left_alt_sep = '⮁'
"let g:airline_right_sep = '⮂'
"let g:airline_right_alt_sep = '⮃'
"let g:airline_symbols.branch = '⭠'
"let g:airline_symbols.readonly = '⭤'
"let g:airline_symbols.linenr = '⭡'

let g:airline_filetype_overrides = {
    \ 'gundo': [ 'Gundo', '' ],
    \ 'help':  [ 'Help', '%f' ],
    \ 'minibufexpl': [ 'MiniBufExplorer', '' ],
    \ 'nerdtree': [ get(g:, 'NERDTreeStatusline', 'NERD'), '' ],
    \ 'startify': [ 'startify', '' ],
    \ 'vim-plug': [ 'Plugins', '' ],
    \ 'vimfiler': [ 'vimfiler', '%{vimfiler#get_status_string()}' ],
    \ 'vimshell': ['vimshell','%{vimshell#get_status_string()}'],
    \ }

