"let g:airline_left_sep = '»'
"let g:airline_left_sep = '▶'
"let g:airline_right_sep = '«'
"let g:airline_right_sep = '◀'
"let g:airline_symbols_crypt = '🔒'
"let g:airline_symbols_linenr = '☰'
"let g:airline_symbols_linenr = '␊'
"let g:airline_symbols_linenr = '␤'
"let g:airline_symbols_linenr = '¶'
"let g:airline_symbols_maxlinenr = ''
"let g:airline_symbols_maxlinenr = '㏑'
"let g:airline_symbols_branch = '⎇'
"let g:airline_symbols_paste = 'ρ'
"let g:airline_symbols_paste = 'Þ'
"let g:airline_symbols_paste = '∥'
"let g:airline_symbols_spell = 'Ꞩ'
"let g:airline_symbols_notexists = 'Ɇ'
"let g:airline_symbols_whitespace = 'Ξ'

" powerline symbols
"let g:airline_left_sep = ''
"let g:airline_left_alt_sep = ''
"let g:airline_right_sep = ''
"let g:airline_right_alt_sep = ''
"let g:airline_symbols_branch = ''
"let g:airline_symbols_readonly = ''
"let g:airline_symbols_linenr = '☰'
"let g:airline_symbols_maxlinenr = ''
"let g:airline_symbols_dirty='⚡'

function! GitBranch()
  return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatuslineGit()
  let l:branchname = GitBranch()
  return strlen(l:branchname) > 0?'  '.l:branchname.' ':''
endfunction

set statusline=
set statusline+=%#PmenuSel#
set statusline+=\           " Blank space
set statusline+=%n 
set statusline+=\           " Blank space
set statusline+=%#LineNr#
set statusline+=
set statusline+=⎇\ 
set statusline+=%{StatuslineGit()}
set statusline+=\           " Blank space
set statusline+=%#LineNr#
set statusline+=
set statusline+=\           " Blank space
set statusline+=\%f
set statusline+=\           " Blank space
set statusline+=%m

"set statusline +=%#PreProc#%{&ff}%*                         " file format
"set statusline +=%#Number#%y%*                              " file type
"set statusline +=%#String#\ %<%t%*                          " full path
"set statusline +=%#SpecialKey#%m%*                          " modified flag
"set statusline +=%#Identifier#%=%5l%*                       " current line
"set statusline +=%#SpecialKey#/%L%*                         " total lines
"set statusline +=%#Identifier#%4v\ %*                       " virtual column number
"set statusline +=%#SpecialKey#0x%04B\ %*                    " character under cursor

set statusline+=%=

set statusline+=%#CursorColumn#
set statusline+=\%y
set statusline+=\           " Blank space
set statusline+=\%{&fileencoding?&fileencoding:&encoding}
set statusline+=\           " Blank space
set statusline+=\[%{&fileformat}\]
set statusline+=\           " Blank space
set statusline+=
set statusline+=\           " Blank space
set statusline+=\%c:%l " line and column
set statusline+=\           " Blank space
set statusline+=%#LineNr#
set statusline+= 
set statusline+=%#PmenuSel#
set statusline+=\           " Blank space
set statusline+=\[%p%%]:[%L]
set statusline+=\           " Blank space
set statusline+=%#LineNr#
