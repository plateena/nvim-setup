" Move cursor
Plug 'easymotion/vim-easymotion'

" Improve the power of %
Plug 'adelarsq/vim-matchit' 

" Mark line to charecter 
Plug 'kshenoy/vim-signature' " Marking the line

Plug 'szw/vim-tags'
set tags=./tags;

let g:tagbar_type_javascript = {
      \ 'ctagstype': 'javascript',
      \ 'kinds': [
      \ 'A:arrays',
      \ 'P:properties',
      \ 'T:tags',
      \ 'O:objects',
      \ 'G:generator functions',
      \ 'F:functions',
      \ 'C:constructors/classes',
      \ 'M:methods',
      \ 'V:variables',
      \ 'I:imports',
      \ 'E:exports',
      \ 'S:styled components',
      \ ]}
" Auto generate tags file
Plug 'ludovicchabant/vim-gutentags'

Plug 'majutsushi/tagbar'
nmap ,t :TagbarToggle<Cr>

