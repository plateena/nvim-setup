Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

Plug 'preservim/nerdtree'

Plug 'tpope/vim-projectionist'        "|

if exists('g:loaded_vim_projectionist_elixir') 
    finish
endif
let g:loaded_vim_projectionist_elixir = 1

let s:base_dir = resolve(expand("<sfile>:p:h"))
let s:proj_jsn = s:base_dir . "/projections.json"

function! s:setProjections()
    let l:json = readfile(s:proj_jsn)
    let g:my_projectionist = projectionist#json_parse(l:json)
    call projectionist#append(getcwd(), g:my_projectionist)
endfunction

autocmd User ProjectionistDetect :call s:setProjections()
