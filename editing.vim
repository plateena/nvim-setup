Plug 'SirVer/ultisnips'         " Track the engine.
Plug 'honza/vim-snippets'       " Snippets are separated from the engine. Add this if you want them:

Plug 'preservim/nerdcommenter'  " For commenting purpose
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1
" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'
" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1
" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1
" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1
" Enable NERDCommenterToggle to check all selected lines is commented or not 
let g:NERDToggleCheckAllLines = 1

Plug 'mattn/emmet-vim'          " Completion using emmet
Plug 'tpope/vim-surround'       " Surround in bracket and other can surround word
Plug 'machakann/vim-sandwich'   " Foe of the vim surround
Plug 'godlygeek/tabular'        " indent the code
Plug 'AndrewRadev/tagalong.vim' " Change the html tag for both end
