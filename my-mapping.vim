let mapleader=" "
" map escape 
imap jk <Esc>

nmap j gj
nmap k gk

" turn off search highlighting with <CR> (carriage-return)
nnoremap <CR> :nohlsearch<CR><CR>

nmap <leader>sv :so ~/.config/nvim/init.vim<Cr> 

nmap <leader>bd :bd<Cr>
nmap <leader>bda :bufdo bd<Cr>

nmap <leader>w :w<Cr>

"cnoreabbr phpcsfix !php-cs-fixer fix %<Cr>

nmap <leader>gs :Gstatus<Cr>

"nnoremap <C-h> <C-W>h
"nnoremap <C-l> <C-W>l
"nnoremap <C-j> <C-W>j
"nnoremap <C-k> <C-W>k

" Terminal movement
tnoremap <A-h> <C-\><C-N><C-w>h
tnoremap <A-j> <C-\><C-N><C-w>j
tnoremap <A-k> <C-\><C-N><C-w>k
tnoremap <A-l> <C-\><C-N><C-w>l
inoremap <A-h> <C-\><C-N><C-w>h
inoremap <A-j> <C-\><C-N><C-w>j
inoremap <A-k> <C-\><C-N><C-w>k
inoremap <A-l> <C-\><C-N><C-w>l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l

nmap <leader>csm :colorscheme random<Cr>
