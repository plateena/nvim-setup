let g:nvim_base_path='~/.config/nvim/'

function! LoadFile(files)
    execute 'source' . g:nvim_base_path . a:files
endfunction


" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
"Plug 'dense-analysis/ale'

call LoadFile('./completion.vim')
call LoadFile('./search.vim')
call LoadFile('./visual.vim')
call LoadFile('./movement.vim')
call LoadFile('./editing.vim')
call LoadFile('./format.vim')
call LoadFile('./php.vim')
call LoadFile('./javascript.vim')
call LoadFile('./helper.vim')
call LoadFile('./documentation.vim')
call LoadFile('./file.vim')

" Initialize plugin system
call plug#end()

call LoadFile('./plug-config/coc.vim')
call LoadFile('./my-mapping.vim')
call LoadFile('./plugin-mapping.vim')
call LoadFile('./statusline-setting.vim')
call LoadFile('./setting.vim')
call LoadFile('./coc-setting.vim')
call LoadFile('./auto-command.vim')
call LoadFile('./plug-config/index.vim')
"call LoadFile('./my-statusline.vim')
