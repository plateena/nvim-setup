Plug 'tobyS/vmustache' " Needed by pdv
Plug 'tobyS/pdv'
Plug 'Rican7/php-doc-modded'

" After phpDoc standard
" let g:pdv_cfg_CommentHead = "/**" 
" let g:pdv_cfg_Comment1 = " * " 
" let g:pdv_cfg_Commentn = " * " 
" let g:pdv_cfg_CommentBlank = " *" 
" let g:pdv_cfg_CommentTail = " */" 
" let g:pdv_cfg_CommentSingle = "//" 
" let g:pdv_cfg_FuncCommentEnd = " // End function" 
" let g:pdv_cfg_ClassCommentEnd = " // End" 
let g:pdv_cfg_VariableTypeTag = "@var" 

" Default values
let g:pdv_cfg_Type = "mixed" 
" let g:pdv_cfg_Package = "" 
" let g:pdv_cfg_Version = "$id$" 
let g:pdv_cfg_Author = "Mohammad Zainundin <plateena711@gmail.com>" 
let g:pdv_cfg_Copyright = strftime('%Y') . " plateena" 
" let g:pdv_cfg_License = "PHP Version 5.4 {@link http://www.php.net/license/}" 

let g:pdv_cfg_ReturnVal = "void" 

" Wether to create tags for class docs or not
let g:pdv_cfg_createClassTags = 1 

" Wether to create @uses tags for implementation of interfaces and inheritance
let g:pdv_cfg_Uses = 1 

" Options
" Whether or not to automatically add the function end comment (1|0)
let g:pdv_cfg_autoEndFunction = 1 

" Whether or not to automatically add the class end comment (1|0)
let g:pdv_cfg_autoEndClass = 1 

" :set paste before documenting (1|0)? Recommended.
let g:pdv_cfg_paste = 1 

" Wether for PHP5 code PHP4 tags should be set, like @access,... (1|0)?
let g:pdv_cfg_php4always = 1 

" Wether to guess scopes after PEAR coding standards:
" $_foo/_bar() == <private|protected> (1|0)?
let g:pdv_cfg_php4guess = 1 

" If you selected 1 for the last value, this scope identifier will be used for
" the identifiers having an _ in the first place.
let g:pdv_cfg_php4guessval = "protected" 

" Whether to generate the following annotations:
let g:pdv_cfg_annotation_Package = 0 
let g:pdv_cfg_annotation_Version = 0 
let g:pdv_cfg_annotation_Author = 1 
let g:pdv_cfg_annotation_Copyright = 1 
let g:pdv_cfg_annotation_License = 0 

nmap <leader>pd :call PhpDocSingle()<Cr>
