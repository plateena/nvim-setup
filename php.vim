call LoadFile('./laravel.vim')

Plug 'phpactor/phpactor', {'for': 'php', 'do': 'composer install'}

" Mapping phpactor
let g:phpactorInputListStrategy = 'phpactor#input#list#fzf'
let g:phpactorQuickfixStrategy = 'phpactor#quickfix#fzf'
nmap <Leader>up :call phpactor#UseAdd()<CR>
nmap <leader>mm :call phpactor#ContextMenu()<Cr>
